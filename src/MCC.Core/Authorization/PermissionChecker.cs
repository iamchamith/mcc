﻿using Abp.Authorization;
using MCC.Authorization.Roles;
using MCC.Authorization.Users;

namespace MCC.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
