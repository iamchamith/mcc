﻿namespace MCC
{
    public class MCCConsts
    {
        public const string LocalizationSourceName = "MCC";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
