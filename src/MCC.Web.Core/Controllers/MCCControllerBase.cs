using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace MCC.Controllers
{
    public abstract class MCCControllerBase: AbpController
    {
        protected MCCControllerBase()
        {
            LocalizationSourceName = MCCConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
