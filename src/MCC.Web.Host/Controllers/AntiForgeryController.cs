using Microsoft.AspNetCore.Antiforgery;
using MCC.Controllers;

namespace MCC.Web.Host.Controllers
{
    public class AntiForgeryController : MCCControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
