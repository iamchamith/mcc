﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MCC.Authorization;

namespace MCC
{
    [DependsOn(
        typeof(MCCCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class MCCApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<MCCAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(MCCApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
