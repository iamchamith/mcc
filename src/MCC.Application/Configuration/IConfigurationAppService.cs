﻿using System.Threading.Tasks;
using MCC.Configuration.Dto;

namespace MCC.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
