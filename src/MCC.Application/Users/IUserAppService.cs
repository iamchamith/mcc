using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MCC.Roles.Dto;
using MCC.Users.Dto;

namespace MCC.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
