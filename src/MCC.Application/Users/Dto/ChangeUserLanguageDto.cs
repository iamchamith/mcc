using System.ComponentModel.DataAnnotations;

namespace MCC.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}