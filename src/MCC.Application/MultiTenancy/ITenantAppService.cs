﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MCC.MultiTenancy.Dto;

namespace MCC.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

