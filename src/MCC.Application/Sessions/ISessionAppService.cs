﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MCC.Sessions.Dto;

namespace MCC.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
