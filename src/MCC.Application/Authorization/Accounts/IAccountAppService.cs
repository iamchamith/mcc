﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MCC.Authorization.Accounts.Dto;

namespace MCC.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
