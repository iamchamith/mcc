﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MCC.Configuration;
using MCC.Web;

namespace MCC.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class MCCDbContextFactory : IDesignTimeDbContextFactory<MCCDbContext>
    {
        public MCCDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MCCDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            MCCDbContextConfigurer.Configure(builder, configuration.GetConnectionString(MCCConsts.ConnectionStringName));

            return new MCCDbContext(builder.Options);
        }
    }
}
