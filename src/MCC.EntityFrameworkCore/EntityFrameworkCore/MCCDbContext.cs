﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using MCC.Authorization.Roles;
using MCC.Authorization.Users;
using MCC.MultiTenancy;

namespace MCC.EntityFrameworkCore
{
    public class MCCDbContext : AbpZeroDbContext<Tenant, Role, User, MCCDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public MCCDbContext(DbContextOptions<MCCDbContext> options)
            : base(options)
        {
        }
    }
}
