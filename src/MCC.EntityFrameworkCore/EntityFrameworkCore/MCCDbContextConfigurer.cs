using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace MCC.EntityFrameworkCore
{
    public static class MCCDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<MCCDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<MCCDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
